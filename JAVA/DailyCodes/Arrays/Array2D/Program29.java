

import java.util.*;

class p29 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int sum = 0;

		System.out.println("Enter Array Size:");
		int size = sc.nextInt();
		
		int arr[] = new int[size];

		for( int i=0; i < arr.length; i++ ) {
			System.out.println("Enter Array Element "+ i);
			arr[i] = sc.nextInt();
		}
		
		
		System.out.println("Elements in Array are: ");

		for( int i=0; i < arr.length;i++) {

			System.out.println(arr[i]);
			sum += arr[i];
		
		}
		System.out.println("Sum of elements of Array are: "+ sum);


	}
}
