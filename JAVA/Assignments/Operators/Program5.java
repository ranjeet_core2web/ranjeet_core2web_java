

class p5 {
	public static void main(String[] args) {
		int num1 = 19;
		int num2 = 5;
		int and = 0b00000001;
		System.out.println(num1 & num2);
		System.out.println(and);
		int or = 0b00010111;	
		System.out.println(num1 | num2);
		System.out.println(or);
		int xor = 0b00010110;
		System.out.println(num1 ^ num2);
		System.out.println(xor);
		
		System.out.println(num1 << 1);
		System.out.println(num1 >> 1);
	}
}
