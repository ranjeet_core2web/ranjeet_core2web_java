


import java.util.*;

class p2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows:");
		int rows = sc.nextInt();
		
		for( int i=1; i<=rows; i++ ) {
			int ch = 'a';
			for( int j=1; j<=i; j++ ) {
				if( i%2 == 1) {
					System.out.print((char)ch++ + " ");
				} else {
					System.out.print("$" + " ");
				}
			}
			System.out.println();
		}

	}
}
