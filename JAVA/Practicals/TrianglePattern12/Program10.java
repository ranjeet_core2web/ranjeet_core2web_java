



import java.util.*;

class p10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int num = 1;
		int ch = 97;
		for( int i=1; i<=rows; i++ ) {
			for( int j=1; j<=i; j++ ) {
				if( i % 2 ==1) {
					System.out.print(num + " ");
				} else {
					System.out.print((char)ch + " ");
				}
				num++;
				ch++;
			}
			System.out.println();
		}


	}
}
