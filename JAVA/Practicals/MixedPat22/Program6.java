


import java.util.*;

class p6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();


		for( int i=1; i<=rows; i++ ) {
			int num = 1;
			for( int j=1; j <= rows-i; j++ ) {	
				System.out.print(" \t");
			}
			for( int k=1; k <= i*2 - 1; k++ ) {
				System.out.print(num + "\t");
				num++;
			}	
			System.out.println();
		}




	}
}
