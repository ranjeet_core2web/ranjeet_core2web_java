


import java.util.*;


class p7 {
	public static void main(String[] args) {
		Scanner rb = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = rb.nextInt();
		
		for( int i=1; i <= rows; i++ ) {
			for( int j=1; j <= rows-i ; j++ ) {
				System.out.print(" \t");	
			}
			int num = i*2 -1;
			for( int k=1; k <= i*2 - 1 ; k++ ) {
				System.out.print(num-- + "\t");
			}
			System.out.println();	
		}
	}
}
