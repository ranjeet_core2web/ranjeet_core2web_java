


import java.util.*;


class p3 {
	public static void main(String[] args) {
		Scanner rb = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = rb.nextInt();
		int num = 1;
		for( int i=1; i<=rows; i++ ) {
			for( int j=1; j<=rows-i; j++ ) {
				System.out.print(" \t");
			}
			
			for( int k=1; k<=i; k++ ) {
				System.out.print(num+"\t");
				num += 3;
			}
			System.out.println();
		}


	}
}
