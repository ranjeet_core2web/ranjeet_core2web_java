

import java.util.*;

class p3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows =  sc.nextInt();
		int num = 1;
		int ch = 64 + rows;
		for( int i=1; i <=rows; i++ ) {
			num = 1;
			ch = 64 + rows;
			for( int j=1; j <=rows; j++ ) {
				if( i % 2 == 1 ) {
					System.out.print((char)ch-- + " ");
				} else { 
					System.out.print(num++ + " ");
				}
		
			}
			System.out.println();
		}



	}
}
