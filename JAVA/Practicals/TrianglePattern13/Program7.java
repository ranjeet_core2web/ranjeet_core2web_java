

import java.util.*;

class p7 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int ch = 95 + rows;
		int num = rows;
		for( int i=rows; i>=1; i-- ) {
			num = i;
			ch = 96 + i;
			for( int j=1; j <=i ; j++ ) {
				if( j % 2 ==1) {
					System.out.print(num + " ");
				} else { 
					System.out.print((char)ch + " ");
				}
				ch--;
				num--;
			}
			System.out.println();
		}



	}
}
