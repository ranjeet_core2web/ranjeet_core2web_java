

import java.util.*;

class p6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();

		for( int i=1; i <=rows; i++) {
			int num =1;
			int ch =97;
			for( int j=rows; j >=i; j-- ) {
				if( j % 2 ==0 ) {
					System.out.print(num++ + " ");
				} else {
					System.out.print((char)ch++ + " " );
				}
			}
			System.out.println();
		}



	}
}
