



import java.util.*;

class p8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int num = 1;
		int ch = 1;


		for( int i=rows; i >=1; i-- ) {
			num = i;
			ch = 64 + i;
			for( int j=1; j <=i; j++ ) {
				if( i % 2 ==1) {
					System.out.print(num-- + " ");
				} else {
					System.out.print((char)ch-- + " ");
				}

			}
			System.out.println();
		}



	}
}
