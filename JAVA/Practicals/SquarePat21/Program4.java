


import java.util.*;


class p4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Rows: ");
		int rows = sc.nextInt();
		int num = rows;
		int ch = 64 + rows;
		for( int i=1; i<=rows; i++ ) {
			for( int j=1; j<=rows; j++ ) {
				if( num % rows == 0 ) {
					System.out.print((char)ch + " ");
				} else {
					System.out.print(num + " ");
				}
				num++;
				ch++;
			}
			System.out.println();
		}


	}
}
